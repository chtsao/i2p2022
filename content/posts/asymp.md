---
title: "Asymptotic Theorems on X-bar"
date: 2023-01-05T04:14:47+08:00
draft: false
categories: [adm, notes]
tags: [Law of Large Numbers, Central Limit Theorem, Ouroboros]
---

<img src="https://s.wsj.net/public/resources/images/RV-AS808A_WILCZ_12U_20160505155129.jpg" style="zoom: 33%;" />

## 公告

期末報告結束之後，I2P 告一段落。**即日起沒有原形式之上課**。但 Game master Kno 會繼續 PO 一至兩次 內容，提供有興趣的玩家探索。

### Asymptotics

這個看起來很偉大的字，在機率與統計中是指  與 樣本數 $n \rightarrow \infty$ 的一些探討, also known as asymptotic analysis, is the describing and study of limiting behaviours of the math/prob/stat objects, for example, $\bar{X}.$  兩個最重要而在理論與應用領域都使用廣泛的定理就是 大數法則 (Law of Large Numbers)   與 中央極限定理 (Central Limit Theorem)。各位可以先在網路或課本上找找他們，讀讀相關的內容，先有個初步的了解，我之後再上傳進一步的解說。

* Text: Chapter 13, Chapter 14. 

* [ChatGPT Dec 15 Ver](https://chat.openai.com/chat). Note that it may not always give the correct answers. But it is fun to check what it says as a starting point. As it confessed  "As an artificial intelligence, I strive to provide accurate and reliable information to the best of my ability. However, it is possible that my  responses may be incomplete or may contain errors. I recommend  fact-checking any information that you receive, including information  from me."

  You may ask the AI-Bot the following questions. 

  * Q1: Explain "Law of Large Numbers". Give me some examples and applications. 
  * Q1': 大數法則是什麼？
  * Q2: What is Central Limit Theorem? How does it relate to Binomial probability calculation?
  * Q2': 中央極限定理是什麼？當樣本數很大時，它如何幫助我們計算 binomial probabilities?

  

  **Kno's Question**: 就這四個問題的答案，其實已有不太正確的部份。對比課本的說明，你可以抓出ChatGPT Aibot 回答中的錯誤嗎？


## Law of Large Numbers

## Central Limit Theorem





