---
title: "Week 5a. Discrete Random Variable-I"
date: 2022-10-10T04:31:52+08:00
draft: false
tags: [random variable, discrete random variable, probability mass function, distribution function, cumulative distribution function]
---
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Random_graffiti_art_%28Unsplash%29.jpg/800px-Random_graffiti_art_%28Unsplash%29.jpg" style="zoom: 75%;" />

After the review/summary of the probability of events, the concept of probability ($P, \Omega, \mathcal{F}$), conditional probability and independence of events, we are now ready to embark on 
$$\Large x \to X.$$

### Random variable 隨機變數
隨機變數可以想成我們所熟悉的變數 (variable)的推廣。在某些意義與限制之下，她似乎也遵從一般變數的代數運算規則，但在本質上的隨機性則是一個重要的差異。她是我們對隨機、未知和不確定性的一種模型。以拋擲一個銅板的結果為例，隨機變數的建模包含了三個部份
1. 可能的結果：正, 反
2. 相對出現的機率： 如是公正銅板，則正反機率各為0.5
3. 量化：(X=1) $\equiv$ 正; (X=0) $\equiv$ 反。這樣的量化，~~不僅環保~~一方面符號上簡潔，也打破了原先在事件/集合上的運算與工具的限制，讓機率的計算回到熟悉的實數空間與面積計算，有微積分等分析與代數工具可以使用/借用，大幅化簡了建構機率/統計理論的工作。

~~絲絲~~隨機變數有兩種, 離散隨機變數與連續隨機變數

### Discrete random variable
所有的離散變數都有以下的架構
#### Probability mass function

> $X$ is a discrete random variable if there exist $a_1, a_2, \cdots, \in \mathcal{R} $ (finitely or countable infinitely many) and a function $f: \mathcal{R} \rightarrow [0, 1]$ such that 
> 
> a.  For any $x \in \mathcal{R},  f(x) \geq 0$ 
> 
> b. $$\sum_{i=1}^\infty f(a_i) = 1.$$

The function $f$ is called probability mass function (pmf) of $X$.
對於離散隨機變數來說, pmf 有(在單點 x)機率的解釋，
$$ f(x)= P(X=x).$$
這裡先提醒，之後要介紹的連續隨機變數所對應的 pdf 並沒有這樣的機率解釋。  

這個函數 pmf 是一個隨機變數的**刻畫** (characterization)。此處刻畫意思是說 pmf 相當於是一個離散隨機變數的身份證或健保卡。要知道她是誰，機率上有什麼行為，只要知道她的 pmf 就可以完全決定。而兩個隨機變數比方叫雷姆、拉姆，但卻有一個完全相同的 pmf, 則表示在機率上來說，她們會有完全相同的性質，機率上完全相同。

對照之前提到隨機變數建模希望達到的三個目的，可能結果/對應機率/量化，一個 pmf （或是 cdf ) 就可達到。

有了 pmf 如何算機率呢？
For any $A \subset \mathcal{R}$
$$ P(X \in A) = \sum_{x \in A} f(x).$$

#### Distribution function
In addition, we can also define a function $F: \mathcal{R} \rightarrow [0, 1] $ as 
$$ F(x) = P(X \leq x) =\sum_{a_i  \leq x} f(a_i). $$
$F$ is called the distribution function (df) or cumulative distribution function (cdf) of $X$.

此函數 $F$ 也是$X$的一個刻畫。再重複一次，這就是說，知道了$F$ cdf 就完全知道這個隨機變數的機率行為。兩個隨機變數有相同 cdf 表示在機率上相同。

### Bernoulli random variable 

### Note

* 由cdf 定義可知，知道 pmf $f$ 即可知道 cdf $F$
* 反過來說，若知道 $F$ 也可將 pmf $f$ 算出。因為 for any $x$
$$ f(x)= F(x) - F(x-)$$
where $F(x-)= \lim_{y \rightarrow x-}F(y)$, the left limit of F at $x$. 