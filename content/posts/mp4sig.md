---
title: "Minimum Prob for Speculators, Investors and Gamblers"
date: 2022-12-06T03:36:18+08:00
draft: false
tags: [gamble's fallacy, St. Petersburg's paradox, Gamblers' Ruin Theorem]
---
![](https://i.pinimg.com/originals/37/f4/56/37f45656061e7cb8e25a10118eb7eef8.jpg)

### [St. Petersburg's Paradox](https://en.wikipedia.org/wiki/St._Petersburg_paradox)

Let $X$ be the reward you get from the following game: We toss a fair coin. If the Head appears on the first toss, you get 2 dollars, if the Head appears for the first time on the $k$-th toss then you get $2^k$ for $k = 2, 3, \ldots$. In probability term, the pmf of $X$ is 
$f(x) = P(X=x)=\frac{1}{x} \quad \rm{if} \ \ x=2^k$ for some $k=1, 2, ....$; o if otherwise. 

Note that f indeed defines a pmf and 

* $E(X) = \infty$.

How much will you pay to play this game?

#### Lesson: Expectation is not what you naively expect.

### [Gambler's fallacy](https://en.wikipedia.org/wiki/Gambler%27s_fallacy)

#### Lesson: Probability is not a realized (or soon-to-be-realized) empirical proportion. 

### [Gambler's Ruin Theorem](https://en.wikipedia.org/wiki/Gambler%27s_ruin)

Consider a coin-flipping game with two players where  Player 1 wins with probability  $p$ with each flip of the coin; Player 2 wins with probability $q=1-p$. After each flip of the coin the loser transfers one penny to the winner. The game ends when one player has all the pennies. Suppose Player 1 starts with $n_1$ and Player 2 starts with $n_2$ pennies. Let $P_i$ be the probability of Player $i$ is ruined (ending with 0 penny).

### Fair coin $p = q = 1/2$

$ P_1 = \frac{n_2}{n_1 +n_2} $  and $P_2 = 1 - P_1.$

### Unfair Coin

$$ P_1 = \frac{1- (p/q)^{n_2}}{1- (p/q)^{n_1 +n_2}}$$  and $P_2 = 1 - P_1.$

#### Lesson 口袋夠深才有機會看到期望值/機率發揮作用

### On Investment
早些規劃自己的財務是好的，但最好先做功課。道聽塗說，人云亦云，被別人牽著鼻子走，很難不變成韭菜。。。
以下是幾個我建議的閱讀，並不技術性，但對建立一個穩健的 mindset 很有幫助。好好讀讀這些內容，細心消化----你可能不會大賺，但應該不會跌到爬不起來。最後但最重要的是--**投資自己**，特別是在各位這個階段。善用時間，成長/開發自己（如能力，心智/思辨，健康，人脈/朋友，感情/家庭，品味，嗜好/娛樂等）。這絕對是你最重要的投資，沒有之一。 

1. [一個投機者的告白 安德烈．科斯托蘭尼](https://www.books.com.tw/products/0010777884)
2. [夠了：約翰‧伯格談金錢的最佳策略](https://www.books.com.tw/products/0010827276?sloc=main)
3. [投資最重要的事：霍華．馬克斯](https://www.books.com.tw/products/0010744933?sloc=main)
4. [股市真規則：晉升專業投資人的五項金律 派特‧多爾西 : Intro + Ch. 1, Ch. 2](https://www.books.com.tw/products/0010761861?sloc=main)
5. [市場先生](https://rich01.com/)
