---
title: "Final Exam"
date: 2022-12-28T07:34:42+08:00
draft: false
tags: [adm, final]

---
<img src="https://www.popdaily.com.tw/shaper/u/202205/169a4438-e312-4e69-a226-98f336e3940c.png?resize-w=900&resize-h=495" style="zoom: 67%;" />

@ [10 Ways to Infuse Your Final Exam Reviews With WICOR](https://avidcollegeready.org/college-career-readiness/2014/12/11/10-ways-to-infuse-your-final-exam-reviews-with-wicor.html)

### Final Exam

* 日期: 2023 0103 (二). 時間: 1310-1440. 
* 地點: 
  * A210: 學號 ≥ 410911273.
  * A212: 學號 < 410911273.

* 範圍：上課及習題內容。對照課本章節約為
  * Chapter 5, Chapter 7; Chapter 8 (Sec 8.3, 8.4 除外)
  * Chapter 9; Chapter 10; Chapter 11 (Sec 11.3 除外). Chapter 13. 
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!
* ~~Extra Office Hours: 1/2 (一). 1310-1500 @A411.~~ Sorry. 忘了是補假。

提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

### 考古題

I2P FINAL(Fixed):  [2020](https://chtsao.gitlab.io/i2p2022/fin2020.pdf), [2021](https://chtsao.gitlab.io/i2p2022/fin2021.pdf)

注意：今年內容包含了 continuous random vector 沒有出現在2019前考題；Chapter 13, Chebyshev's inequality + Law of Large Numbers 也是今年才上到的內容。但都是考試的重要主題。2019年前的考古題參考價值較低，沒有提供。請自行針對上課筆記、課本習題內容練習。總之是要會，不要背。好好參考[Study less, study smart](https://chtsao.gitlab.io/i2p2022/posts/hello2/#study-less-study-smart)的幾個原則如 Quizz+Space+Mix. 加油！