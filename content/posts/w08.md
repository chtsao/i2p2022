---
title: "Week 8. E(g(X)): Expectation, Variation and Moments"
date: 2022-11-01T07:06:14+08:00
draft: false
categories: [notes, adm]
tags: [expectation, variation, moments]
---

<img src="http://dldxedu.com/wp-content/uploads/2016/08/The-exams-are-approaching-the-panic.jpg" style="zoom:100%;" />

## Midterm Exam

* 日期: 2022 1108 (二)
* 時間: 1310-1440. 
* Exam Room: TBA.
* 範圍：上課及習題內容。對照課本章節約為: Chapter 2, Chapter 3,  Chapter 4, Chapter 5 (除 Sec 5.5, Sec 5.6 外) 以及 Chapter 7 中的 $E(X), Var(X), E(g(X))$. 
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!
* [Midterm 2020](https://chtsao.gitlab.io/i2p2022/mid2020.pdf), [Midterm 2021](https://chtsao.gitlab.io/i2p2022/mid2021.pdf) 考古。(Skip the mgf $M(t)$ questions.) 

## Expectation, Variation and E(g(X)) for discrete r.v.

Let $X$ be a discrete random variable with pmf $f$. Then 

* $E(X) = \sum_{x: f(x)>0} x f(x)$ if exists.
* $E(g(X)) = \sum_{x: f(x)>0} g(x) f(x)$ if exists.
* $Var(X) = E(X- E(X))^2 = \sum_{x: f(x)>0} (x-E(X))^2 f(x)$ if exists.
* For any $a, b \in  \mathcal{R}$
  * $E(aX +b) =  a E(X) +b.$ 
  * $Var(aX+b) = a^2 Var(X).$
* For any $a, b \in  \mathcal{R}$ and $g(x), h(x)$, $E(a g(X) + b h(X)) = a E(g(X)) + b E(h(X))$ if exist.
* $Var(X)= E(X- E(X))^2 = E(X^2 - 2X E(X) + (E(X))^2 ) = E(X^2) - (E(X))^2$

### Computing Expectation and Variance

* $X \sim Bernoulli(p)$
* $X \sim Bin(n, p)$
* $X \sim DU(K)$
* $E(X)$ when $X \sim Geo(p)$

