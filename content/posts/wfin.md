---
title: "Sum of iid random variabbles"
date: 2022-12-28T07:34:33+08:00
draft: false
categories: [notes]
---

### Expectation of functions of independent random variables
Let $X_1, \cdots, X_n$ are independent random variables and (Borel measurable) functions $g_1, \cdots, g_n$. Then

$E(\prod_{i=1}^n g_i (X_i)) = \prod_{i=1}^n E(g_i (X_i)) $
provided $E(g_i (X_i))$ exists for all $i$.

Particularly, take $g_i(x) = e^{tx}$ then $E(g_i (X_i)) = M_{X_i}(t)$, the moment generating function of $X_i$. That is


$M_{\sum_{i=1}^n X_i}(t) = E(\prod_{i=1}^n e^{t X_i}) = \prod_{i=1}^n M_{X_i}(t) $
if $M_{X_i}(t)$ exists for all $i$.

### Distribution of sum of iid random variables

* Let $X_1, \cdots, X_n \sim_{iid} Bernoulli(p), p \in (0,1)$ and $p+q=1$. Then
 $\sum_{i=1}^n X_i \sim Bin(n, p).$
* Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ then 
   * $\sum_{i=1}^n X_i \sim N( n \mu, n \sigma^2)$.
   * $\bar{X} = \frac{1}{n}\sum_{i=1}^n X_i \sim N( \mu,  \sigma^2/n)$.