---
title: "About"
date: 2022-09-06T15:04:21+08:00
draft: false
categories: [adm]
---
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ff4.bcbits.com%2Fimg%2Fa3083993486_5.jpg&f=1&nofb=1" alt="Uncertainty" style="zoom:33%;" />


* [Syllabus](https://chtsao.gitlab.io/i2p2022/i2p22.sylla.pdf).    Curator: C. Andy (Kno) Tsao  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1700 
  * 3D: @ A210.; 
  * Virtual: [Google Class](https://classroom.google.com/c/NTQ1ODMwMzY4MjEw?cjc=iqn2muc), [Google Meet](https://meet.google.com/dou-muwq-xvk)
* Office Hours:  MON 13:10-14:00；THU 15:10-16:00 @ SE A411 or by appointment.
* TA Office Hours: (NEW)
  * [呂一昕](mailto:610911007@gms.ndhu.edu.tw): Thr. 17:00-19:00 @ SE A412
  * [蘇羿豪](mailto:611011102@gms.ndhu.edu.tw): Tue. 15:00-16:00, Thr. 17:00-18:00. @SE A311
* Prerequisites: Calculus and curiosity about uncertainty/randomness
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)
